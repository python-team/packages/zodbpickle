.manylinux-install.sh
.manylinux.sh
.pre-commit-config.yaml
.readthedocs.yaml
CHANGES.rst
CONTRIBUTING.md
LICENSE.txt
MANIFEST.in
README.rst
buildout.cfg
pyproject.toml
setup.cfg
setup.py
tox.ini
docs/Makefile
docs/conf.py
docs/index.rst
docs/make.bat
docs/historical/proposal.rst
patches/pickle_bytes_code.diff
patches/pickle_bytes_tests.diff
patches/pickle_noload.patch
src/zodbpickle/__init__.py
src/zodbpickle/_pickle_33.c
src/zodbpickle/fastpickle.py
src/zodbpickle/pickle.py
src/zodbpickle/pickle_3.py
src/zodbpickle/pickletools_3.py
src/zodbpickle/slowpickle.py
src/zodbpickle.egg-info/PKG-INFO
src/zodbpickle.egg-info/SOURCES.txt
src/zodbpickle.egg-info/dependency_links.txt
src/zodbpickle.egg-info/not-zip-safe
src/zodbpickle.egg-info/requires.txt
src/zodbpickle.egg-info/top_level.txt
src/zodbpickle/tests/__init__.py
src/zodbpickle/tests/pickle_3_tests.py
src/zodbpickle/tests/pickletester_3.py
src/zodbpickle/tests/test_compile_flags.py
src/zodbpickle/tests/test_pickle.py